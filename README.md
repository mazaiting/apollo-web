<p align="center">
    <img src="https://img.shields.io/badge/Vue-3.2.39-brightgreen.svg" alt="vue 版本"/>
    <img src="https://img.shields.io/badge/Vite-3.1.3-green.svg" alt="vite 版本"/>
    <img src="https://img.shields.io/badge/Element Plus-2.2.17-blue.svg" alt="Element plus版本"/>
    <a href="https://gitee.com/mazaiting/apollo-web" target="_blank"></a> 
    <br/>
    <img src="https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg" alt="协议版本"/>
    <a href="https://gitee.com/youlaiorg" target="_blank">
        <img src="https://img.shields.io/badge/author-mazaiting-orange" alt="作者"/>
    </a>
</p>

## 项目介绍

[vue3-element-admin](https://gitee.com/youlaiorg/vue3-element-admin) 是基于 [vue-element-admin](https://gitee.com/panjiachen/vue-element-admin) 升级的 Vue3 版本后台管理前端解决方案；使用前端主流技术栈 Vue3 + Vite2 + TypeScript + Vue Router + Pinia + Volar + Element Plus 等；实现功能包括不限于动态权限路由、按钮权限控制、国际化、主题大小切换等；[有来商城](https://gitee.com/youlaitech/youlai-mall-admin) 基于此模板生成, 而阿波罗管理系统主要基于有来商城管理系统改造。项目内容主要来源于[有来商城](https://gitee.com/youlaitech/youlai-mall-admin), 感谢有来团队的无私奉献。

## 项目优势

- 基于 vue-element-admin 升级的 Vue3 版本 ，极易上手，减少学习成本；
- 一套完整适配的微服务权限系统线上接口，企业级真实前后端接入场景，非 Mock 数据；
- 功能全面：国际化、动态路由、按钮权限、主题大小切换、Echarts、wangEditor；
- TypeScript 全面支持，包括组件和 API 调用层面；
- 主流 Vue3 生态和前端技术栈，常用组件极简封装；
- 从 0 到 1 的项目文档支持；
- 全栈技术支持: 微服务接口、Vue3 管理前端、uni-app 移动端和 K8S 持续集成交付；

## 技术栈

| 技术栈          | 描述                     | 官网                                   |
|--------------|------------------------|--------------------------------------|
| Vue3         | 渐进式 JavaScript 框架      | https://v3.cn.vuejs.org/             |
| TypeScript   | JavaScript 的一个超集       | https://www.tslang.cn/               |
| Vite2        | 前端开发与构建工具              | https://cn.vitejs.dev/               |
| Element Plus | 基于 Vue 3，面向设计师和开发者的组件库 | https://element-plus.gitee.io/zh-CN/ |
| Pinia        | 新一代状态管理工具              | https://pinia.vuejs.org/             |
| Vue Router   | Vue.js 的官方路由           | https://router.vuejs.org/zh/         |

## 项目地址

|                    | Gitee                                                                | Github                                                                 | GitCode                                      |
|--------------------|----------------------------------------------------------------------|------------------------------------------------------------------------|----------------------------------------------|
| 开源组织               | [有来开源组织](https://gitee.com/youlaiorg)                                | [有来开源组织](https://github.com/youlaitech)                                | [有来开源组织](https://gitcode.net/youlai)         || 技术团队               | [有来技术团队](https://gitee.com/youlaitech)                               | [有来技术团队](https://github.com/youlaitech)                                | -                                                           |
| 后端                 | [apollo](https://gitee.com/mazaiting)                                | [apollo](https://github.com/mazaiting)                                 | [apollo](https://gitee.com/mazaiting/apollo) |
| vue3-element-admin | [vue3-element-admin](https://gitee.com/youlaiorg/vue3-element-admin) | [vue3-element-admin](https://github.com/youlaitech/vue3-element-admin) | -                                            |

## 启动部署

### 环境准备

- 安装 Node

  版本：v14 或 v16

- 开发工具

  推荐 WebStorm


### 项目启动

> 需要修改 vite.config.ts 的代理地址 http://localhost:9000 为自己的代理地址 http://localhost:9000 

1. npm install
2. npm run dev
3. 浏览器访问 http://localhost:9528

🚨 如果安装依赖或启动报错，可尝试将依赖包 `/docs/node_modules.tar.gz` 解压到项目根目录。

🚨 如果因为网络问题导致 npm install 安装依赖很慢可更换为淘宝镜像源

```bash
# 设置镜像源地址为淘宝
npm config set registry https://registry.npm.taobao.org
# 确认更换是否成功
npm config get registry
# 继续安装
npm install
```

### 项目部署

- 本地打包

  ```
  npm run build:prod
  ```

  生成的静态文件位于项目根目录 dist 文件夹下

- nginx.cofig 配置

  ```
  server {
      listen     80;
      server_name  localhost;

      location / {
          root /usr/share/nginx/html/web;
          index index.html index.htm;
      }

      # 代理转发请求至网关，prod-api标识解决跨域问题
      location /prod-api/ {
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_pass https://api.youlai.tech/;
      }
  }

  ```

## 工具
- [Vue调试工具](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=zh-CN)
- [角标生成工具](https://shields.io/)
- [图标库](https://www.iconfont.cn/)

#### Project Progress
- 2022 年 10 月 15 日: feat: 完善 sentinel 限流功能
- 2022 年 10 月 12 日: feat: 新增用户 excel 上传下载功能; fix: 修复 token 失效自动刷新及重新登录问题
- 2022 年 10 月 10 日: 完成 minio 上传文件实验功能, 下载文件功能
- 2022 年 10 月 04 日: 增加短信验证码, 图形验证码登录
- 2022 年 09 月 25 日: 根据有来团队项目重构项目权限管理
