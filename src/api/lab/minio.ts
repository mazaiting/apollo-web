import {deleted, downloadWithBuffer, get, post, upload} from '@/utils/requestNew';
import {BucketQueryParam, BucketVO, FileInfo, FileUploadVO, ObjectFileVO, UploadPolicyVO} from "@/types/api/lab/minio";
import {AxiosPromise} from "@/types/common";

/**
 * 列出所有桶
 * @param param 查询参数
 */
export function listBucketPages(param: BucketQueryParam) {
  return get('/lab/v1/minio/listBuckets', param);
}

/**
 * 创建存储桶
 * @param data 数据
 */
export function createBucket(data: BucketVO) {
  return post('/lab/v1/minio/createBucket', data);
}

/**
 * 删除存储桶
 * @param bucketName 存储桶名称
 */
export function deleteBucket(bucketName: string) {
  return deleted(`/lab/v1/minio/deleteBucket/${bucketName}`);
}

/**
 * 上传文件
 */
export function uploadFile(data: FileUploadVO) {
  return upload('/lab/v1/minio/uploadFile', data);
}

/**
 * 获取临时授权
 */
export function policy(data: ObjectFileVO) {
  return get('/lab/v1/minio/policy', data);
}

/**
 * 根据临时授权上传
 * @param uploadPolicyVO 上传授权 VO
 * @param file 文件
 */
export function uploadPolicy(uploadPolicyVO: UploadPolicyVO, file: File) {
  return post(uploadPolicyVO.host, {
      file: file,
      key: uploadPolicyVO.key,
      policy: uploadPolicyVO.policy,
      "x-amz-algorithm": uploadPolicyVO.xamzAlgorithm,
      "x-amz-credential": uploadPolicyVO.xamzCredential,
      "x-amz-date": uploadPolicyVO.xamzDate,
      "x-amz-signature": uploadPolicyVO.xamzSignature
    },
    {"content-type": "multipart/form-data"})
}

/**
 * 桶文件列表
 * @param bucketName 桶名
 */
export function listFiles(bucketName: string): AxiosPromise<FileInfo[]> {
  return get(`/lab/v1/minio/listFiles/${bucketName}`);
}

/**
 * 下载文件
 * @param objectFileVO 对象文件 VO
 */
export function download(objectFileVO: ObjectFileVO) {
  return downloadWithBuffer('/lab/v1/minio/download', objectFileVO);
}

/**
 * 删除文件
 * @param objectFileVO 对象文件 VO
 */
export function deleteFile(objectFileVO: ObjectFileVO) {
  return deleted('/lab/v1/minio/deleteFile', objectFileVO);
}

