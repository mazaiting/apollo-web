import {get} from "@/utils/requestNew";
import {AxiosPromise} from "@/types/common";

/**
 * 【普通流控】获取数据
 * @returns
 */
export function getFlowLimitingData(): AxiosPromise<string> {
  return get('lab/v1/sentinel/flowLimiting');
}

/**
 * 【网关流控-RouteID】获取数据
 * @returns
 */
export function getGatewayRouteFlowLimitingData(): AxiosPromise<string> {
  return get('lab/v1/sentinel/gatewayRouteFlowLimiting');
}

/**
 * 【网关流控-API分组】获取数据
 * @returns
 */
export function getGatewayApiFlowLimitingData(): AxiosPromise<string> {
  return get('lab/v1/sentinel/gatewayApiFlowLimiting');
}
