import {
  Captcha,
  LoginFormData,
  LoginResponseData,
} from '@/types/api/system/login';
import {deleted, get, post} from "@/utils/requestNew";
import store from "@/store";
import {ElMessage} from "element-plus";
import {AxiosPromise} from "@/types/common";

/**
 * 登录
 * @param data 登录信息
 */
export function login(data: LoginFormData): AxiosPromise<LoginResponseData> {
  let reqData = {};
  if (data.grantType === 'password') {
    // 密码登录
    reqData = {
      username: data.username.trim(),
      password: data.password
    }
  } else if (data.grantType === 'captcha') {
    // 图形验证码
    reqData = {
      username: data.username.trim(),
      password: data.password,
      code: data.code,
      uuid: data.uuid
    }
  } else if (data.grantType === 'sms_code') {
    // 短信验证码
    reqData = {
      mobile: data.phone.trim(),
      code: data.code
    }
  } else {
    ElMessage.error('无效授权类型')
  }
  return post(
    'auth/v1/oauth/login',
    // data,
    // {
    //   oauth_client: 'Basic Q0xJRU5UX1JPT1Q6JDJhJDEwJGJKZ0k4UEhVajJVaVRlL2s4aHNLYy5mL3Y1VWp3SDZqZEtVUzJCUHZtSlVLajZEdDBEd1R5', // 客户端信息Base64明文：CLIENT_ROOT:$2a$10$bJgI8PHUj2UiTe/k8hsKc.f/v5UjwH6jdKUS2BPvmJUKj6Dt0DwTy
    //   grant_type: 'password'
    // },
    // data,
    // {
    //   oauth_client: 'Basic Q0xJRU5UX1JPT1Q6JDJhJDEwJGJKZ0k4UEhVajJVaVRlL2s4aHNLYy5mL3Y1VWp3SDZqZEtVUzJCUHZtSlVLajZEdDBEd1R5', // 客户端信息Base64明文：CLIENT_ROOT:$2a$10$bJgI8PHUj2UiTe/k8hsKc.f/v5UjwH6jdKUS2BPvmJUKj6Dt0DwTy
    //   grant_type: 'captcha'
    // },
    // {
    //   mobile: "17700000000",
    //   code: "666666"
    // },
    reqData,
    {
      oauth_client: 'Basic Q0xJRU5UX1JPT1Q6JDJhJDEwJGJKZ0k4UEhVajJVaVRlL2s4aHNLYy5mL3Y1VWp3SDZqZEtVUzJCUHZtSlVLajZEdDBEd1R5', // 客户端信息Base64明文：CLIENT_ROOT:$2a$10$bJgI8PHUj2UiTe/k8hsKc.f/v5UjwH6jdKUS2BPvmJUKj6Dt0DwTy
      grant_type: data.grantType
    },
  );
}


/**
 * 刷新 token
 */
export function refresh(): AxiosPromise<LoginResponseData> {
  return post('auth/v1/oauth/refresh',
    {},
    {
      oauth_client: 'Basic Q0xJRU5UX1JPT1Q6JDJhJDEwJGJKZ0k4UEhVajJVaVRlL2s4aHNLYy5mL3Y1VWp3SDZqZEtVUzJCUHZtSlVLajZEdDBEd1R5', // 客户端信息Base64明文：CLIENT_ROOT:$2a$10$bJgI8PHUj2UiTe/k8hsKc.f/v5UjwH6jdKUS2BPvmJUKj6Dt0DwTy
      grant_type: 'refresh_token',
      refresh_token: store().user.refreshToken
    }
    // {
    //   ...data,
    //   grantType: 'refresh_token',
    //   clientId: 'CLIENT_ADMIN',
    //   clientSecret: '123456',
    //   refreshToken: store.getters.refreshToken
    // })
  )
}

/**
 * 注销
 */
export function logout() {
  return deleted('auth/v1/oauth/logout');
}

/**
 * 获取图片验证码
 */
export function getCaptcha(): AxiosPromise<Captcha> {
  return get('uac/v1/captcha?t=' + new Date().getTime().toString());
}

/**
 * 获取登录验证码
 * @param phone 手机号
 */
export function getLoginSmsCode(phone: string) {
  return post('uac/v1/sms?t=' + new Date().getTime().toString(), {
    phone: phone,
    platform: 1,
    type: 1
  })
}
