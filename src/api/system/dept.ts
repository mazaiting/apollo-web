import {
  DeptFormData,
  DeptItem,
  DeptQueryParam,
} from '@/types/api/system/dept';
import {AxiosPromise, Option} from '@/types/common';
import { get, post, put, deleted } from '@/utils/requestNew';

/**
 * 部门树形表格
 *
 * @param queryParams
 */
export function listDepartments(
  queryParams?: DeptQueryParam
): AxiosPromise<DeptItem[]> {
  return get('/manager/v1/depts', queryParams);
}

/**
 * 部门下拉列表
 */
export function listDeptOptions(): AxiosPromise<Option[]> {
  return get('manager/v1/depts/options');
}

/**
 * 获取部门详情
 *
 * @param id 部门 ID
 */
export function getDeptDetail(id: string): AxiosPromise<DeptFormData> {
  return get(`manager/v1/depts/${id}`);
}

/**
 * 新增部门
 *
 * @param data
 */
export function addDept(data: DeptFormData) {
  return post('manager/v1/depts', data);
}

/**
 *  修改部门
 *
 * @param data 部门数据
 */
export function updateDept(data: DeptFormData) {
  return put('manager/v1/depts', data);
}

/**
 * 删除部门
 *
 * @param ids id 数组
 */
export function deleteDept(ids: string) {
  return deleted(`manager/v1/depts/${ids}`);
}
