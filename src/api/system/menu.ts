import {
  MenuFormData,
  MenuItem,
  MenuQueryParam,
  Resource,
} from '@/types/api/system/menu';
import {AxiosPromise, Option} from '@/types/common';
import {deleted, get, post, put} from '@/utils/requestNew';

/**
 * 获取路由列表
 */
export function listRoutes() {
  return get('manager/v1/menus/routes');
}

/**
 * 获取菜单表格列表
 *
 * @param queryParams 查询参数
 */
export function listMenus(queryParams: MenuQueryParam): AxiosPromise<MenuItem[]> {
  return get('manager/v1/menus', queryParams);
}

/**
 * 获取菜单下拉树形列表
 */
export function listMenuOptions(): AxiosPromise<Option[]> {
  return get('manager/v1/menus/options');
}

/**
 * 获取资源(菜单+权限)树形列表
 */
export function listResources(): AxiosPromise<Resource[]> {
  return get('manager/v1/menus/resources');
}

/**
 * 获取菜单详情
 * @param menuId 菜单 ID
 */
export function getMenuDetail(menuId: string): AxiosPromise<MenuFormData> {
  return get(`manager/v1/menus/${menuId}`);
}

/**
 * 添加菜单
 *
 * @param data 菜单数据
 */
export function addMenu(data: MenuFormData) {
  return post('manager/v1/menus', data);
}

/**
 * 修改菜单
 *
 * @param data 菜单表单数据
 */
export function updateMenu(data: MenuFormData) {
  return put('manager/v1/menus', data);
}

/**
 * 批量删除菜单
 *
 * @param ids 菜单ID，多个以英文逗号(,)分割
 */
export function deleteMenus(ids: string) {
  return deleted(`manager/v1/menus/${ids}`);
}
