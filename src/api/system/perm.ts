import {
  PermFormData,
  PermItem,
  PermPageResult,
  PermQueryParam,
} from '@/types/api/system/perm';
import request from '@/utils/request';
import {deleted, get, post, put} from "@/utils/requestNew";
import {AxiosPromise} from "@/types/common";

/**
 * 获取权限分页列表
 *
 * @param queryParams 查询参数
 */
export function listPermPages(queryParams: PermQueryParam): AxiosPromise<PermPageResult> {
  return get('manager/v1/permissions/page', queryParams);
}

/**
 * 获取权限详情
 *
 * @param id 权限 ID
 */
export function getPermFormDetail(id: number): AxiosPromise<PermFormData> {
  return get(`manager/v1/permissions/${id}`);
}

/**
 * 添加权限
 *
 * @param data 权限数据
 */
export function addPerm(data: PermFormData) {
  return post('manager/v1/permissions', data);
}

/**
 * 更新权限
 *
 * @param data 权限数据
 */
export function updatePerm(data: PermFormData) {
  return put('manager/v1/permissions', data);
}

/**
 * 批量删除权限，多个以英文逗号(,)分割
 *
 * @param ids 权限 ID 列表
 */
export function deletePerms(ids: string) {
  return deleted(`manager/v1/permissions/${ids}`);
}
