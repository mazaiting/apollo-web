import {
  RoleFormData,
  RolePageResult,
  RoleQueryParam,
  RoleResource,
} from '@/types/api/system/role';

import {AxiosPromise, Option} from '@/types/common';
import {deleted, get, post, put} from '@/utils/requestNew';

/**
 * 获取角色分页数据
 *
 * @param queryParams 查询参数
 */
export function listRolePages(queryParams?: RoleQueryParam): AxiosPromise<RolePageResult> {
  return get('manager/v1/roles/pages', queryParams);
}

/**
 * 获取角色下拉数据
 *
 * @param queryParams
 */
export function listRoleOptions(queryParams?: RoleQueryParam): AxiosPromise<Option[]> {
  return get('manager/v1/roles/options', queryParams);
}

/**
 * 获取角色拥有的资源ID集合
 *
 * @param roleId 角色 ID
 */
export function getRoleResources(roleId: number): AxiosPromise<RoleResource> {
  return get(`manager/v1/roles/${roleId}/resources`);
}

/**
 * 修改角色资源权限
 *
 * @param roleId 角色 ID
 * @param data 资源
 */
export function updateRoleResource(roleId: string, data: RoleResource): AxiosPromise<any> {
  return put(`manager/v1/roles/${roleId}/resources`, data);
}

/**
 * 获取角色详情
 *
 * @param roleId 角色 ID
 */
export function getRoleFormDetail(roleId: string): AxiosPromise<RoleFormData> {
  return get(`manager/v1/roles/${roleId}`);
}

/**
 * 添加角色
 *
 * @param data 角色表单数据
 */
export function addRole(data: RoleFormData) {
  return post('manager/v1/roles', data);
}

/**
 * 编辑角色
 *
 * @param data 角色表单数据
 */
export function updateRole(data: RoleFormData) {
  return put('manager/v1/roles', data);
}

/**
 * 批量删除角色，多个以英文逗号(,)分割
 *
 * @param ids 角色 ID 列表
 */
export function deleteRoles(ids: string) {
  return deleted(`manager/v1/roles/${ids}`);
}
