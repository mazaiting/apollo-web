import {get, put, post, deleted, patch, downloadWithBuffer, upload} from '@/utils/requestNew';
import {
  UserFormData,
  UserInfo,
  UserPageResult,
  UserQueryParam,
} from '@/types/api/system/user';
import {AxiosPromise} from "@/types/common";

/**
 * 登录成功后获取用户信息（昵称、头像、权限集合和角色集合）
 */
export function getUserInfo(): AxiosPromise<UserInfo> {
  return get('manager/v1/users/me');
}

/**
 * 获取用户分页列表
 *
 * @param queryParams
 */
export function listUserPages(
  queryParams: UserQueryParam
): AxiosPromise<UserPageResult> {
  return get('/manager/v1/users/pages', queryParams);
}

/**
 * 获取用户表单详情
 *
 * @param userId
 */
export function getUserDetail(userId: number): AxiosPromise<UserFormData> {
  return get(`manager/v1/users/${userId}`);
}

/**
 * 添加用户
 *
 * @param data 表单数据
 */
export function addUser(data: UserFormData) {
  return post('manager/v1/users', data);
}

/**
 * 修改用户
 *
 * @param data 表单数据
 */
export function updateUser(data: UserFormData) {
  return put('manager/v1/users', data);
}

/**
 * 修改用户状态
 *
 * @param userId 用户 ID
 * @param enabled 用户状态
 */
export function updateUserEnabled(userId: number, enabled: boolean) {
  return patch(`manager/v1/users/${userId}/enabled`, {enabled: enabled});
}

/**
 * 修改用户密码
 *
 * @param userId 用户 ID
 * @param password 密码
 */
export function updatePassword(userId: number, password: string) {
  return patch(`manager/v1/users/${userId}/password`, {password: password});
}

/**
 * 删除用户
 *
 * @param ids 用户 ID
 */
export function deleteUsers(ids: string) {
  return deleted(`manager/v1/users/${ids}`);
}

/**
 * 下载用户导入模板
 *
 * @returns
 */
export function downloadTemplate() {
  return downloadWithBuffer('manager/v1/users/template');
}

/**
 * 导出用户
 *
 * @param queryParams 查询参数
 * @returns 用户文件
 */
export function exportUser(queryParams: UserQueryParam) {
  return downloadWithBuffer('manager/v1/users/export', queryParams);
}

/**
 * 导入用户
 *
 * @param deptId 部门 ID
 * @param roleIds 角色 ID
 * @param file 文件
 */
export function importUser(deptId: number, roleIds: number[], file: File) {
  return upload('manager/v1/users/import', {
    file: file,
    deptId: deptId,
    roleIds: roleIds
  });
}
