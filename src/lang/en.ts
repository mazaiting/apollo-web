export default {
  // 路由国际化
  route: {
    dashboard: 'Dashboard',
    document: 'Document',
  },
  // 登录页面国际化
  login: {
    title: 'apollo',
    username: 'Username',
    password: 'Password',
    login: 'Login',
    code: 'Verification Code',
    mobile: 'mobile',
    copyright: 'Copyright © 2021 - 2022 mazaiting All Rights Reserved. ',
    icp: '',
  },
  // 导航栏国际化
  navbar: {
    dashboard: 'Dashboard',
    logout: 'Logout',
    document: 'Document',
    gitee: 'Gitee',
  },
};
