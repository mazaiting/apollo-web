export default {
  // 路由国际化
  route: {
    dashboard: '首页',
    document: '项目文档',
  },
  // 登录页面国际化
  login: {
    title: '阿波罗管理系统',
    username: '用户名',
    password: '密码',
    login: '登 录',
    code: '请输入验证码',
    mobile: '手机号',
    copyright:
      'Copyright © 2022 - gitee.com All Rights Reserved. mazaiting 版权所有',
    icp: 'ICP备案号:新ICP备20220000号-0',
  },
  navbar: {
    dashboard: '首页',
    logout: '注销',
    document: '项目文档',
    gitee: '码云',
  },
};
