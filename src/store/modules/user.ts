import { defineStore } from 'pinia';
import { LoginFormData } from '@/types/api/system/login';
import { UserState } from '@/types/store/user';

import { localStorage } from '@/utils/storage';
import { login, logout } from '@/api/login';
import { getUserInfo } from '@/api/system/user';
import { resetRouter } from '@/router';

const useUserStore = defineStore({
  id: 'user',
  state: (): UserState => ({
    token: localStorage.get('token') || '',
    refreshToken: localStorage.get('refreshToken') || '',
    nickname: '',
    avatar: '',
    roles: [],
    perms: [],
  }),
  actions: {
    async RESET_STATE() {
      this.$reset();
    },
    /**
     * 登录
     */
    login(loginData: LoginFormData) {
      const { username, password, code, uuid } = loginData;
      return new Promise((resolve, reject) => {
        login(loginData)
          .then((response) => {
            console.log(response.data)
            const { access_token, token_type, refresh_token } = response.data;
            const accessToken = token_type + ' ' + access_token;
            localStorage.set('token', accessToken);
            localStorage.set('refreshToken', refresh_token)
            this.token = accessToken;
            this.refreshToken = refresh_token
            resolve(access_token);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    /**
     *  获取用户信息（昵称、头像、角色集合、权限集合）
     */
    getUserInfo() {
      return new Promise((resolve, reject) => {
        getUserInfo()
          .then((result) => {
            console.log(result)
            const data = result.data
            if (!data) {
              return reject('认证失败, 请重新登录');
            }
            const { nickname, roles, perms } = data;
            if (!roles || roles.length <= 0) {
              reject('该用户暂未分配角色, 请联系管理员');
            }
            this.nickname = nickname;
            this.avatar = 'https://s2.loli.net/2022/04/07/gw1L2Z5sPtS8GIl.gif';
            this.roles = roles;
            this.perms = perms;
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    /**
     *  注销
     */
    logout() {
      return new Promise((resolve, reject) => {
        logout()
          .then(() => {
            localStorage.remove('token');
            localStorage.remove('refreshToken');
            this.RESET_STATE().then(() => {});
            resetRouter();
            resolve(null);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    /**
     * 清除 Token
     */
    async resetToken() {
      return new Promise((resolve) => {
        localStorage.remove('token');
        localStorage.remove('refreshToken');
        this.RESET_STATE();
        resolve(null);
      });
    },
  },
});

export default useUserStore;
