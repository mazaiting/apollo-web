/**
 * 分页查询
 */
export interface PageQueryParam {
  pageNum: number;
  pageSize: number;
}

/**
 * 查询结果
 */
export interface PageResult<T> {
  list: T;
  total: number;
}
