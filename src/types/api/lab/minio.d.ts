import {PageQueryParam} from "@/types/api/base";

/**
 * 存储桶查询参数
 */
export interface BucketQueryParam extends PageQueryParam {
  name?: string;
}


/**
 * 存储桶
 */
export interface BucketVO {
  name: string;
  createDate?: string;
}

/**
 * 文件上传
 */
export interface FileUploadVO {
  // 文件
  file: File;
  // 存储桶名
  bucketName: string;
}

/**
 * 对象文件 VO
 */
export interface ObjectFileVO {
  // 存储桶名
  bucketName: string;
  // 对象名
  objectName: string;
}

/**
 * 上传临时授权 VO
 */
export interface UploadPolicyVO {
  // 主机
  host: string;
  // 文件路径
  key: string;
  // 授权
  policy: string;
  // 算法
  xamzAlgorithm: string;
  // 授信
  xamzCredential: string;
  // 日期
  xamzDate: string;
  // 签名
  xamzSignature: string;
}

/**
 * 文件信息
 */
export interface FileInfo {
  // 文件名
  fileName: string;
  // 是否为路径
  directory: string;
}
