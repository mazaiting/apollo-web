/**
 * 登录表单类型声明
 */
export interface LoginFormData {
  // 用户名
  username: string;
  // 密码
  password: string;
  // 授权类型
  grantType: string;
  // 验证码
  code: string;
  // uuid
  uuid: string;
  // 手机号
  phone: string;
}

/**
 * 登录响应类型声明
 */
export interface LoginResponseData {
  access_token: string;
  token_type: string;
  refresh_token: string;
}

/**
 * 验证码类型声明
 */
export interface Captcha {
  img: string;
  uuid: string;
}
