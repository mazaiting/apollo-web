/**
 * 弹窗类型
 */
export interface Dialog {
  title: string;
  visible: boolean;
}

/**
 * 通用组件选择项类型
 */
export interface Option {
  value: string;
  label: string;
  checked?: boolean;
  children?: Option[];
}


/**
 * 统一返回
 */
export interface Result<T> {
  // 响应码
  code: number;
  // 异常信息
  msg: string;
  // 数据
  data: T;
}

export type AxiosPromise<T> = Promise<Result<T>>
