export interface UserState {
  token: string;
  refreshToken: string;
  nickname: string;
  avatar: string;
  roles: string[];
  perms: string[];
}
