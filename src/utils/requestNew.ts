import axios, {AxiosRequestConfig, AxiosRequestHeaders, AxiosResponse, ResponseType} from 'axios';
import {ElMessage, ElMessageBox} from 'element-plus';
import {localStorage} from '@/utils/storage';
import useStore from '@/store';
import router from "@/router";
import {refresh} from "@/api/login";
import store from "@/store";
import {AxiosPromise} from "@/types/common";

// 是否正在刷新的标记
let isRefreshing = false
// 重试队列，每一项将是一个待执行的函数形式
let requests: any[] = []

// 创建 axios 实例
const service = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 30000,
  headers: {'Content-Type': 'application/json;charset=utf-8'},
});

// 请求拦截器
service.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (!config.headers) {
      throw new Error(
        `Expected 'config' and 'config.headers' not to be undefined`
      );
    }
    console.log(config)
    console.log('=====================请求拦截器开始=============================')
    console.log('请求地址: ' + config.url)
    const {user} = useStore();
    console.log('token: ' + user.token)
    if (user.token) {
      config.headers.Authorization = `${localStorage.get('token')}`;
    }
    console.log('=====================请求拦截器结束=============================')
    return config;
  },
  (error) => {
    console.log('请求异常' + error) // for debug
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response: AxiosResponse) => {
    console.log('=====================响应拦截器开始=============================')
    console.log('响应接口地址: ' + response.config.url)
    // 响应码 response.status
    // 配置 response.config
    // 数据 response.data
    // 头部信息 response.headers
    console.log(response)
    const status = response.status
    console.log('状态码: ' + status)

    if (status === 200) {
      const data = response.data
      // 响应数据为二进制流处理(Excel导出)
      const {code} = data;
      // 如果不为 1 说明请求失败了
      if (code !== 1) {
        // 如果是流数据则下载
        if (data instanceof ArrayBuffer || data instanceof Blob) {
          handleDownload(response)
          return data;
        }
        console.log('响应数据: ' + JSON.stringify(data))
        ElMessage({
          message: data.msg || '系统出错',
          type: 'error',
          duration: 5 * 1000
        });
        return Promise.reject(data);
      } else {
        console.log('响应数据: ' + JSON.stringify(data))
        // 响应成功返回数据
        return data
      }
    } else if (status === 204) {
      // minio 根据临时授权上传成功时返回 204
      return response.data
    } else {
      console.log('响应异常: ' + status)
    }
  },
  (error) => {
    console.log('=====================响应拦截器异常=============================')
    console.log('响应异常接口: ' + error.config.url)
    // 判断是否存在响应码
    const url = error.config.url
    // 如果是获取图形验证码接口, 异常什么也不做
    if (url.includes('uac/v1/captcha')) {
      return Promise.reject()
    }
    // 如果是登录接口, 则直接跳转至登录页
    if (url.includes('auth/v1/oauth/logout') || url.includes('auth/v1/oauth/refresh')) {
      ElMessageBox.alert('当前页面已失效，请重新登录', '提示', {
        confirmButtonText: '确定',
        callback: () => {
          // // token 过期
          router.push('/login').then(() => {
            // token 过期
            localStorage.clear(); // 清除浏览器全部缓存
            window.location.href = '/'; // 跳转登录页
            //   ElMessageBox.alert('当前页面已失效，请重新登录', '提示', {}).then();
          })
        },
      }).then()
      // return Promise.reject()
    } else {
      console.log(error.config)
      console.log(error)
      console.log(error.message)
      const message = error.message
      let toastMsg = message
      if (message.includes('Network Error')) {
        toastMsg = '无网络'
      } else {
        console.log(error.response.status)
        // 响应内容
        const response = error.response
        // 响应码
        const status = response.status
        switch (status) {
          case 500:
          case 501:
          case 502:
          case 503:
            toastMsg = '服务不可用'
            break
          case 400:
            toastMsg = '服务器异常'
            break
          case 401:
            toastMsg = '登录已失效'
            break
          case 403:
            toastMsg = '无操作权限, 请联系管理员开通'
            break
          case 404:
            toastMsg = '资源未发现'
            break
        }
        if (message.includes('timeout')) {
          toastMsg = '访问超时'
        }
        // console.log(error.response.status)
        if (error.response.status) {
          switch (error.response.status) {
            case 401: {
              // 获取401失败请求的axios中的config配置数据
              const config = error.config
              console.log('config: ')
              console.log(config)
              // 没有刷新
              if (!isRefreshing) {
                // 设置为正在刷新
                isRefreshing = true
                // 请求刷新token的接口
                return refresh().then((response) => {
                  console.log('token过期刷新接口调用成功')
                  console.log(response)
                  // 无论是否有触发异常，该语句都会执行
                  isRefreshing = false
                  // @ts-ignore
                  if (response.code === 1) {
                    const {access_token, token_type, refresh_token} = response.data;
                    // 重置 token, 刷新状态
                    store().user.resetToken().then(() => {
                      const accessToken = token_type + ' ' + access_token;
                      localStorage.set('token', accessToken);
                      localStorage.set('refreshToken', refresh_token)
                      store().user.token = accessToken;
                      store().user.refreshToken = refresh_token
                    }).finally(() => {
                      console.log('请求条数: ' + requests.length)
                      // 已经刷新了token，将所有队列中的请求进行重试
                      requests.forEach(cb => cb(response.data.access_token))
                      requests = []
                    })
                    return saveAndRetryRequest(config)
                  } else {
                    showReLoginMessage()
                    // ElMessage.error('登录已过期, 请重新登录')
                  }
                })
                //   .catch((err: Error) => {
                //   // 无论是否有触发异常，该语句都会执行
                //   isRefreshing = false
                //   console.error('refresh_token error =>', err)
                //   // 跳转到登录页
                //   // router.push('/login').then()
                //   showReLoginMessage()
                // })
                  .finally(() => {
                  // 无论是否有触发异常，该语句都会执行
                  isRefreshing = false
                })
              } else {
                return saveAndRetryRequest(config)
              }
            }
            case 429:
              // sentinel 流量治理
              return Promise.resolve(response.data);
            default:
              break;
          }
        }
      }
      return Promise.reject(new Error(toastMsg || 'Error'));
    }


    //   const { code, msg } = error.response.data;
    // if (code === 'A0230') {
    //   // token 过期
    //   localStorage.clear(); // 清除浏览器全部缓存
    //   window.location.href = '/'; // 跳转登录页
    //   ElMessageBox.alert('当前页面已失效，请重新登录', '提示', {});
    // } else if (code == 'B0210') {
    //   // 系统限流
    //   return error.response.data;
    // } else {
    //   ElMessage({
    //     message: msg || '系统出错',
    //     type: 'error',
    //   });
    // }
    // return Promise.reject(new Error(msg || 'Error'));
  }
);

/**
 * 显示重新登录对话框
 */
const showReLoginMessage = () => {
  ElMessageBox.alert('当前页面已失效，请重新登录', '提示', {
    confirmButtonText: '确定',
    callback: () => {
      // // token 过期
      router.push('/login').then(() => {
        // token 过期
        localStorage.clear(); // 清除浏览器全部缓存
        window.location.href = '/'; // 跳转登录页
        //   ElMessageBox.alert('当前页面已失效，请重新登录', '提示', {}).then();
      })
    },
  }).then()
}


/**
 * 保存并且重发请求
 * @param config 请求配置
 */
const saveAndRetryRequest = (config: AxiosRequestConfig) => {
  // 正在刷新token，将返回一个未执行resolve的promise
  // 保存函数 等待执行
  // 把请求都保存起来 等刷新完成后再一个一个调用
  return new Promise((resolve) => {
    // 将resolve放进队列，用一个函数形式来保存，等token刷新后直接执行
    requests.push((accessToken: string) => {
      console.log('重试: ' + accessToken)
      // config.headers!.Authorization = `${localStorage.get('token')}`;
      config.headers!.Authorization = accessToken;
      resolve(request(config))
    })
  })
}

/**
 * 处理下载文件
 * @param response 响应
 */
function handleDownload(response: AxiosResponse) {
  // 创建 二进制 数据
  const blob = new Blob([response.data], {type: response.headers['content-type']});
  const a = document.createElement('a');
  // 下载的链接
  const href = window.URL.createObjectURL(blob);
  a.href = href;
  // 获取后台设置的文件名称, 去除文件名前后的双引号
  let fileName = response.headers['content-disposition'].split(';')[1].split('=')[1];
  if (fileName.startsWith("\"")) {
    fileName = fileName.substring(1)
  }
  if (fileName.endsWith("\"")) {
    fileName = fileName.substring(0, fileName.length - 1)
  }
  // 解码 URI
  a.download = decodeURI(fileName);
  document.body.appendChild(a);
  // 点击导出
  a.click();
  // 下载完成移除元素
  document.body.removeChild(a);
  // 释放掉blob对象
  window.URL.revokeObjectURL(href);
}

/**
 * get 请求
 * @param url 请求地址
 * @param params 请求数据
 * @param headers 请求头
 */
const get = <T>(url: string, params: object = {}, headers: AxiosRequestHeaders = {}): AxiosPromise<T> =>
  service({
    url: url,
    headers,
    method: 'get',
    params: {...params}
  }).then(result => {
    return result as any
  })

/**
 * post 请求
 * @param url 请求地址
 * @param data 请求数据
 * @param headers 请求头
 */
const post = <T>(url: string, data: object = {}, headers: AxiosRequestHeaders = {}): AxiosPromise<T> => service({
  url: url,
  headers,
  method: 'post',
  data
}).then(result => {
  return result as any
})

/**
 * delete 请求
 * @param url 请求地址
 * @param params 请求数据
 * @param headers 请求头
 */
const deleted = <T>(url: string, params: object = {}, headers: AxiosRequestHeaders = {}): AxiosPromise<T> => service({
  url: url,
  headers,
  method: 'delete',
  params
}).then(result => {
  return result as any
})

/**
 * put 请求
 * @param url 请求地址
 * @param data 请求数据
 * @param headers 请求头
 */
const put = <T>(url: string, data: object = {}, headers: AxiosRequestHeaders = {}): AxiosPromise<T> => service({
  url: url,
  headers,
  method: 'put',
  data
}).then(result => {
  return result as any
})

/**
 * patch 请求
 * @param url 请求地址
 * @param data 请求数据
 * @param headers 请求头
 */
const patch = <T>(url: string, data: object = {}, headers: AxiosRequestHeaders = {}): AxiosPromise<T> => service({
  url: url,
  headers,
  method: 'patch',
  data
}).then(result => {
  return result as any
})

/**
 * 请求
 */
const request = (params: object = {}) => service(params)

/**
 * post 请求
 * @param url 请求地址
 * @param data 请求数据
 */
const upload = <T>(url: string, data: object = {}): AxiosPromise<T> => service({
  url: url,
  headers: {"content-type": "multipart/form-data"},
  method: 'post',
  data
}).then(result => {
  return result as any
})

/**
 * 下载文件
 * @param url 下载地址
 * @param params 数据
 * @param responseType 响应类型
 */
const download = (url: string, params: object = {}, responseType?: ResponseType) => service({
  url: url,
  method: 'get',
  params,
  responseType
})

// /**
//  * 下载文件
//  * @param url 下载地址
//  * @param params 数据
//  */
// const downloadByBlob = (url: string, params: object = {}) => service({
//   url: url,
//   method: 'get',
//   params,
//   responseType: 'blob'
// })

/**
 * 下载文件
 * @param url 下载地址
 * @param params 数据
 */
const downloadWithBuffer = (url: string, params: object = {}) => download(url, params, 'arraybuffer')

// 导出 axios 实例
export {
  get,
  post,
  deleted,
  put,
  patch,
  request,
  upload,
  download,
  downloadWithBuffer
}

